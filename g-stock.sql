-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Dim 14 avr. 2019 à 19:03
-- Version du serveur :  5.7.24
-- Version de PHP :  7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `g-stock`
--

-- --------------------------------------------------------

--
-- Structure de la table `approvisionnement`
--

DROP TABLE IF EXISTS `approvisionnement`;
CREATE TABLE IF NOT EXISTS `approvisionnement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_prod` int(11) NOT NULL,
  `id_four` int(11) NOT NULL,
  `quantite` int(11) NOT NULL,
  `date_ap` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `approvisionnement`
--

INSERT INTO `approvisionnement` (`id`, `id_prod`, `id_four`, `quantite`, `date_ap`) VALUES
(6, 16, 1, 4, '2019-04-13 08:40:49'),
(7, 16, 1, 5, '2019-04-13 08:42:26'),
(8, 16, 1, 5, '2019-04-13 08:43:46'),
(9, 16, 1, 5, '2019-04-13 08:43:56'),
(10, 16, 1, 5, '2019-04-13 08:44:04'),
(11, 16, 1, 5, '2019-04-13 08:57:25'),
(12, 0, 1, 1, '2019-04-13 08:58:09'),
(13, 0, 1, 1, '2019-04-13 09:00:08'),
(14, 0, 1, 1, '2019-04-13 09:00:11'),
(15, 0, 1, 1, '2019-04-13 09:00:50'),
(16, 0, 1, 1, '2019-04-13 09:03:49'),
(17, 16, 1, 15, '2019-04-13 09:04:35'),
(18, 16, 1, 1, '2019-04-13 09:06:01'),
(19, 16, 1, 100, '2019-04-13 09:07:04'),
(20, 16, 1, 100, '2019-04-13 09:17:02'),
(21, 16, 1, 100, '2019-04-13 09:18:49'),
(22, 16, 1, 100, '2019-04-13 09:18:57'),
(23, 16, 1, 100, '2019-04-13 09:19:26'),
(24, 16, 1, 100, '2019-04-13 09:19:29'),
(25, 4, 2, 5, '2019-04-14 12:12:06'),
(26, 5, 2, 12, '2019-04-14 16:28:51'),
(27, 6, 1, 7, '2019-04-14 16:59:12'),
(28, 7, 4, 3, '2019-04-14 17:12:10'),
(29, 8, 5, 3, '2019-04-14 17:18:07'),
(30, 10, 5, 18, '2019-04-14 17:28:57'),
(31, 11, 7, 11, '2019-04-14 17:44:55'),
(32, 12, 8, 13, '2019-04-14 18:13:42'),
(33, 13, 8, 30, '2019-04-14 18:14:18'),
(34, 14, 9, 5, '2019-04-14 18:14:56'),
(35, 15, 8, 12, '2019-04-14 18:15:21'),
(36, 16, 9, 8, '2019-04-14 18:15:50'),
(37, 17, 10, 5, '2019-04-14 18:16:09');

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nom` (`nom`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`id`, `nom`) VALUES
(10, 'Alimentaires'),
(7, 'Boisons & caves Ã  vin'),
(9, 'ElectromÃ©nager, Electronique et Informatique'),
(8, 'tÃ©lÃ©phones & accessoires'),
(6, 'TV , son & hifi');

-- --------------------------------------------------------

--
-- Structure de la table `clients`
--

DROP TABLE IF EXISTS `clients`;
CREATE TABLE IF NOT EXISTS `clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(225) NOT NULL,
  `prenom` varchar(225) NOT NULL,
  `tel` varchar(8) NOT NULL,
  `email` varchar(225) NOT NULL,
  `motdepasse` varchar(225) NOT NULL,
  `date_inscription` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `etat` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `clients`
--

INSERT INTO `clients` (`id`, `nom`, `prenom`, `tel`, `email`, `motdepasse`, `date_inscription`, `etat`) VALUES
(1, 'guelade', 'annicette', '01254578', 'annicetteguelade@gmail.com', '123456', '2019-04-14 12:14:20', 1),
(4, 'guelade', 'audrey', '45568975', 'audreyguelade@gmail.com', '123456', '2019-04-14 12:27:30', 1),
(7, 'kouassi', 'loraine', '04554545', 'loraine@gmail.com', '123456', '2019-04-14 12:28:46', 0);

-- --------------------------------------------------------

--
-- Structure de la table `commandes`
--

DROP TABLE IF EXISTS `commandes`;
CREATE TABLE IF NOT EXISTS `commandes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_client` int(11) NOT NULL,
  `id_prod` int(11) NOT NULL,
  `quantite` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `confirme` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `commandes`
--

INSERT INTO `commandes` (`id`, `id_client`, `id_prod`, `quantite`, `date`, `confirme`) VALUES
(1, 1, 4, 5, '2019-04-14 14:11:52', 1),
(2, 1, 4, 5, '2019-04-14 14:38:00', 1),
(3, 1, 5, 1, '2019-04-14 18:46:31', 0);

-- --------------------------------------------------------

--
-- Structure de la table `factures`
--

DROP TABLE IF EXISTS `factures`;
CREATE TABLE IF NOT EXISTS `factures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(11) NOT NULL,
  `id_cmd` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `fournisseurs`
--

DROP TABLE IF EXISTS `fournisseurs`;
CREATE TABLE IF NOT EXISTS `fournisseurs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `tel` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nom` (`nom`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `fournisseurs`
--

INSERT INTO `fournisseurs` (`id`, `nom`, `email`, `tel`) VALUES
(1, 'nescle', 'contact@nescle.om', '04458565'),
(2, 'solibra', 'contact@solibra.ci', '04458565'),
(4, 'Awa', 'awa_ci@gmail.com', '09080700'),
(5, 'princesse', 'pci@princesse.ci', '07060809'),
(6, 'rama', 'rama@mail.ci', '01010203'),
(7, 'Toplait', 'toplaitci@gmail.com', '05050605'),
(8, 'Nasco', 'Nasco@ci.com', '01020304'),
(9, 'Samsung', 'Samsungci@gmail.com', '02020302'),
(10, 'Huawei', 'Huawei_ci@gmail.com', '02020304');

-- --------------------------------------------------------

--
-- Structure de la table `historique_connexion`
--

DROP TABLE IF EXISTS `historique_connexion`;
CREATE TABLE IF NOT EXISTS `historique_connexion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_utilisateur` int(11) NOT NULL,
  `derniere_c` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `historique_connexion`
--

INSERT INTO `historique_connexion` (`id`, `id_utilisateur`, `derniere_c`) VALUES
(11, 1, '10-04-2019 15:55'),
(10, 2, '10-04-2019 15:55');

-- --------------------------------------------------------

--
-- Structure de la table `marques`
--

DROP TABLE IF EXISTS `marques`;
CREATE TABLE IF NOT EXISTS `marques` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nom` (`nom`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `marques`
--

INSERT INTO `marques` (`id`, `nom`) VALUES
(8, 'Awa'),
(4, 'Ã©lÃ©ctro-mÃ©nager'),
(3, 'Coca Cola'),
(6, 'Fanta'),
(14, 'Huawei'),
(12, 'Nasco'),
(9, 'Princesse'),
(7, 'Rama'),
(13, 'Samsung'),
(1, 'sivop'),
(2, 'smart'),
(5, 'TÃ©lÃ©visions'),
(11, 'Toplait');

-- --------------------------------------------------------

--
-- Structure de la table `produits`
--

DROP TABLE IF EXISTS `produits`;
CREATE TABLE IF NOT EXISTS `produits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_marque` int(11) NOT NULL,
  `id_categorie` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prix_vente` int(11) NOT NULL,
  `prix_achat` int(11) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `quantite` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `nom` (`nom`),
  KEY `id_categorie` (`id_categorie`),
  KEY `produits_ibfk_2` (`id_marque`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `produits`
--

INSERT INTO `produits` (`id`, `id_marque`, `id_categorie`, `nom`, `prix_vente`, `prix_achat`, `photo`, `description`, `quantite`) VALUES
(4, 3, 7, 'coca cola zero', 300, 250, 'coca2.jpg', 'bouteille canette', 3),
(5, 6, 7, 'Fanta Orange', 300, 250, 'fanta.jpg', 'Bouteille plastique\r\n30 cl', 12),
(7, 8, 7, 'Eau Awa', 450, 300, 'awa.jpg', 'Awa   Eau minÃ©rale naturelle  1,5 L\r\n', 3),
(8, 9, 10, 'Sardine', 450, 400, 'sardine.png', 'Sardine princesse\r\n125 ml', 3),
(10, 2, 10, 'Tomate pÃ¢te', 100, 90, 'topchef.jpg', 'Tomate pÃ¢te Top chef - 70g', 18),
(11, 11, 10, 'Laity', 100, 80, 'laity.jpg', 'Lait en Poudre laity Nature - sachet - 22g', 11),
(12, 2, 9, 'NASCO Ventilateur ', 18000, 16000, 'ventilateur.jpg', 'NASCO Ventilateur Ã  piedâ€“ VENT_FD-40MC', 13),
(13, 2, 9, 'Montre connectÃ©', 10000, 8000, 'montre2.jpg', ' A1 smartwatch montre tÃ©lÃ©phone connectÃ©e Ã©quipÃ©e dâ€™un Ã©cran tactile qui offre une rÃ©solution de 240 x 240 pixels. ', 30),
(14, 2, 8, 'casque Audio', 15000, 12500, 'casque.jpg', 'Casque avec Microphone Sur L\'oreille Casque StÃ©rÃ©o Basse Big Ã‰couteurs pour PC Portable Android tÃ©lÃ©phone ', 5),
(15, 12, 6, 'TÃ©lÃ©viseur   NASCO  32â€™ ', 160000, 150000, 'Tv.jpg', 'TÃ©lÃ©viseur LED - NASCO - 32â€™â€™ â€“ LED_E32C1NA_SMART  - HD - HDMI/USB/VGA/AV - DÃ©codeur IntÃ©grÃ©  - Wifi', 12),
(16, 13, 8, 'Samsung S8 Duos ', 350000, 320000, 'samsungs8.jpg', 'Smartphone - Samsung Galaxy - S8 Duos - 64Go/4Go - Double SIM - 12MP/8MP - 2.3 GHz - Noir', 8),
(17, 14, 8, 'Huawei P20', 180000, 160000, 'huaweip20.png', 'Huawei P20 - 128Go - RAM4Go - 5.8\" - 20Mp/12Mp - Noir', 5);

-- --------------------------------------------------------

--
-- Structure de la table `type_utilisateur`
--

DROP TABLE IF EXISTS `type_utilisateur`;
CREATE TABLE IF NOT EXISTS `type_utilisateur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nom` (`nom`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `type_utilisateur`
--

INSERT INTO `type_utilisateur` (`id`, `nom`) VALUES
(1, 'admin'),
(2, 'caissier'),
(3, 'manager');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--

DROP TABLE IF EXISTS `utilisateurs`;
CREATE TABLE IF NOT EXISTS `utilisateurs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_type_utilisateur` int(11) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `prenom` varchar(100) NOT NULL,
  `img` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `motdepasse` varchar(255) NOT NULL,
  `date_ins` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `derniere_co` varchar(50) NOT NULL DEFAULT 'Aucune connexion',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='table du personnel et administrateurs';

--
-- Déchargement des données de la table `utilisateurs`
--

INSERT INTO `utilisateurs` (`id`, `id_type_utilisateur`, `nom`, `prenom`, `img`, `email`, `motdepasse`, `date_ins`, `derniere_co`) VALUES
(1, 1, 'guelade', 'kevin', 'Guelade.jpg', 'kevinguelade@gmail.com', '123456', '2019-04-08 02:02:40', '14-04-2019 Ã  15:05'),
(2, 1, 'christian', 'kouman', 'kouman.jpg', 'christianleof@gmail.com', '123456', '2019-04-09 13:02:07', '14-04-2019 Ã  18:48'),
(3, 1, 'ble ', 'anderson', 'ble.jpg', 'esolinble40@gmail.com', '1234', '2019-04-09 13:04:32', 'Aucune connexion'),
(5, 1, 'Orlane', 'Akouba', 'orlane.jpeg', 'orlane05@gmail.com', '123456', '2019-04-10 17:00:55', 'Aucune connexion');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `produits`
--
ALTER TABLE `produits`
  ADD CONSTRAINT `produits_ibfk_1` FOREIGN KEY (`id_categorie`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `produits_ibfk_2` FOREIGN KEY (`id_marque`) REFERENCES `marques` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
