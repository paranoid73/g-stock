<?php
/*recuperer la liste des categories*/
$categorie = $requete->from('categories')->fetchAll();

?>
<!DOCTYPE html>
<html>
<head>
    <title>g-stock - ecommerce</title>
    <link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all"/>
    <!--theme-style-->
    <link href="assets/css/style2.css" rel="stylesheet" type="text/css" media="all"/>
    <link rel="stylesheet" href="assets/css/etalage.css" type="text/css" media="all"/>
    <!--//theme-style-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <script type="application/x-javascript"> addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        } </script>
    <!--fonts-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
    <!--//fonts-->
    <script src="assets/js/jquery.min.js"></script>

    <script src="assets/js/jquery.etalage.min.js"></script>
    <script>
        jQuery(document).ready(function ($) {

            $('#etalage').etalage({
                thumb_image_width: 300,
                thumb_image_height: 400,
                source_image_width: 900,
                source_image_height: 1200,
                show_hint: true,
                click_callback: function (image_anchor, instance_id) {
                    alert('Callback example:\nYou clicked on an image with the anchor: "' + image_anchor + '"\n(in Etalage instance: "' + instance_id + '")');
                }
            });

        });
    </script>

</head>
<body>
<!--header-->
<div class="header">
    <div class="bottom-header">
        <div class="container">
            <div class="header-bottom-left">
                <div class="logo">
                    <a href="index.php"><img src="assets/images/logo.png" width="110" alt=" "/></a>
                </div>
                <div class="search">
                    <input type="text" placeholder="vous recherchez ?">
                    <input type="submit" value="RECHERCHE">

                </div>
                <div class="clearfix"></div>
            </div>
            <div class="header-bottom-right">

                <ul class="login">
                    <?php if(!isset($_SESSION['client'])): ?>
                    <li><a href="index.php?p=connexion"><span> </span>connexion</a></li>
                    |
                    <li><a href="index.php?p=inscription">s'inscrire</a></li>
                    <?php else: ?>
                        <li><a href="index.php?p=deconnexion">se deconnecter</a></li>
                    <?php endif; ?>
                </ul>
                <div class="cart"><a href="index.php?p=panier"><span> </span>
                        <?php
                            if(isset($_SESSION["cart"]))
                                echo count(array_keys($_SESSION["cart"]));
                            else {
                                echo "(0)";
                            }
                        ?>
                    </a></div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!---->

<div class="container">
    <div class="row">
        <div class="col-lg-4">
            <h3>CATEGORIES</h3>
            <ul class="menu">

                <ul class="kid-menu ">
                    <?php foreach ($categorie as $cat): ?>
                        <li><a href="index.php?p=categorie&id=<?= $cat['id'] ?>&nom=<?= $cat['nom'] ?>"><?= $cat['nom'] ?></a></li>
                    <?php endforeach; ?>
                </ul>

            </ul>
        </div>
        <!--initiate accordion-->
        <script type="text/javascript">
            $(function () {
                var menu_ul = $('.menu > li > ul'),
                    menu_a = $('.menu > li > a');
                menu_ul.hide();
                menu_a.click(function (e) {
                    e.preventDefault();
                    if (!$(this).hasClass('active')) {
                        menu_a.removeClass('active');
                        menu_ul.filter(':visible').slideUp('normal');
                        $(this).addClass('active').next().stop(true, true).slideDown('normal');
                    } else {
                        $(this).removeClass('active');
                        $(this).next().stop(true, true).slideUp('normal');
                    }
                });

            });
        </script>

        <div class="col-lg-8">
            <?php echo $content; ?>
        </div>
    </div>
</div>
<!---->
<div class="footer">
    <div class="footer-top">
        <div class="container">
            <div class="latter">
                <h6>NEWS-LETTER</h6>
                <div class="sub-left-right">
                    <form>
                        <input type="text" placeholder="votre adresse email"/>
                        <input type="submit" value="S'ABONNER"/>
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="latter-right">
                <p>FOLLOW US</p>
                <ul class="face-in-to">
                    <li><a href="#"><span> </span></a></li>
                    <li><a href="#"><span class="facebook-in"> </span></a></li>
                    <div class="clearfix"></div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="text-center">
                <h4>G-STOCK - votre boutique en ligne et près de vous</h4>
            </div>
        </div>
    </div>
</div>
</body>
</html>