<?php

if (isset($_GET['id'])) {

    $id = (int)$_GET['id'];
    $produit = $requete->from('produits')->where('id', $id)->fetch();

}

?>

<div class="grid images_3_of_2">
    <ul id="etalage">
        <li>
            <a href="#">
                <input id="img" type="hidden" value="<?= $produit['photo'] ?>">
                <img class="etalage_thumb_image" src="assets/images/produits/<?= $produit['photo'] ?>"
                     class="img-responsive"/>
                <img class="etalage_source_image" src="assets/images/produits/<?= $produit['photo'] ?>"
                     class="img-responsive" title=""/>
            </a>
        </li>
    </ul>
</div>
<div class="desc1 span_3_of_2">

    <h4 id="nom"> <?= $produit['nom'] ?></h4>
    <div class="cart-b">
        <div class="left-n "><span id="prix"><?= $produit['prix_vente'] ?></span> F CFA
        </div>
        <a href="cart.php?id_prod=<?= $produit['id'] ?>&action=ajouter"
           class="btn btn-primary border-0 now-get get-cart-in">Ajouer au
            panier
        </a>
        <div class="clearfix"></div>
    </div>
    <h6><?= '[ '.$produit['quantite'].' ] de disponible' ?> </h6>


    <p><?= $produit['description'] ?>.</p>

    <div class="share">
        <h5>Partagez un produit :</h5>
        <ul class="share_nav">
            <li><a href="#"><img src="assets/images/facebook.png" title="facebook"></a></li>
            <li><a href="#"><img src="assets/images/twitter.png" title="Twiiter"></a></li>
        </ul>
    </div>
</div>
