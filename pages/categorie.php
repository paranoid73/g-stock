<?php
if(isset($_GET['id']) && !empty($_GET['id'])){
    $id = (int) $_GET['id'];
    $produits = $requete->from('produits pro')
                ->innerJoin('categories ca ON pro.id_categorie = ca.id')
                ->select('ca.nom as nom_ca')
                ->where('ca.id',$id) ->fetchAll();
}
?>
<!---->

<div class="products">
    <h5 class="latest-product">Les dernières nouveautés - <?= $_GET['nom'] ?></h5>
    <a class="view-all" href="index.php">VOIR TOUT<span> </span></a>
</div>
<div class="product-left">
    <?php foreach ($produits as $pr): ?>
        <div class="col-md-4">
            <a href="index.php?p=produit&id=<?= $pr['id'] ?>"><img class="img-responsive chain"
                                                                   src="assets/images/produits/<?= $pr['photo'] ?>"
                                                                   alt=" "/></a>
            <span class="star"> </span>
            <div class="grid-chain-bottom">
                <h6><a href="index.php?p=produit&id=<?= $pr['id'] ?>"><?= $pr['nom'] ?></a></h6>
                <div class="star-price">
                    <div class="dolor-grid">
                        <span class="actual"><?= $pr['prix_vente'] ?>F CFA</span>
                    </div>
                    <a class="now-get get-cart" href="index.php?p=produit&id=<?= $pr['id'] ?>">Acheter</a>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>


