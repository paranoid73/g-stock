<?php

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    extract($_POST);

    $message = [];
    /*verification de l'adresse email */
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $message['erreur'] = "l'adresse email n'est pas valide";
    }

    if (empty($message)) {
        $reponse = $requete->from("clients")->where('email',$email)->where('motdepasse',$motdepasse)->fetch();

        if(!empty($reponse)){
            if($reponse['etat'] == 0){
                $message['erreur'] = "votre compte est desactivé , veuillez contacter l'administration du site";
            }else{
                $_SESSION['client'] = $reponse;
                header('location:index.php');
            }
        }else{
            $message['erreur'] = "email ou mot de passe non valide , veuillez réessayer  .";
        }

    }

}

?>




<div class="account_grid">
    <div class="login-right">
        <h3>Connexion à votre compte</h3>
        <?php if(isset($message['erreur']) && !empty($message['erreur'])): ?>
                        <p class="text-danger text-capitalize ">
                            <?= $message['erreur']; ?>
                        </p>
                        <?php endif; ?>
        <form method="post">
            <div>
                <span>Email Address<label>*</label></span>
                <input type="email" name="email" required>
            </div>
            <div>
                <span>mot de passe<label>*</label></span>
                <input type="password" name="motdepasse" required>
            </div>
            <a class="forgot" href="#">mot de passe oubliée ?</a>
            <input type="submit" value="Connexion">

        </form>
    </div>
    <div class=" ogin-left">
        <h3>Nouveau client ?</h3>
        <p>En créeant votre compte , vous aurez la possibilité d'utiliser notre service en ligne de commander , plus rapidement , d'obtenir une assistance en ligne</p>
        <a class="acount-btn" href="index.php?p=inscription">Créer un compte</a>
    </div>
    <div class="clearfix"></div>
</div>


<!---->
