<?php

if(!isset($_SESSION['client']) || empty($_SESSION['client'])){
    header('location:index.php?p=connexion');
    exit();
}

if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    if (isset($_GET['action']) && $_GET['action'] == 'supprimer' && !empty($_GET['id_prod'])) {
        $id = (int)$_GET['id_prod'];
        if (isset($_SESSION['cart'][$id])) {
            unset($_SESSION['cart'][$id]);
        }
    }

}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    extract($_POST);

    foreach ($id_prod as $key => $value){
        $tableau = ['id_client' => $_SESSION['client']['id'],
                    'id_prod'=> $key,
                    'quantite' => $qte[$key],
                    'confirme' => 0 ];
        $reponse = (bool) $requete->insertInto('commandes',$tableau)->execute();
        if($reponse){
            header('location:index.php?p=success');
            unset($_SESSION['cart']);
            exit();
        }
    }
}


$total_price = 0;
?>
<div class="products">
    <?php if (isset($_SESSION["cart"]) && !empty($_SESSION["cart"])): ?>
        <form method="post">
            <table class="table-responsive table table-bordered">
                <tbody>
                <tr>
                    <td></td>
                    <td>Nom</td>
                    <td>quantite</td>
                    <td>prix unitaire</td>
                </tr>
                <?php $ids = array_keys($_SESSION['cart']); ?>
                <?php
                if (empty($ids)) {
                    $products = [];
                } else {
                    $d = implode(',', $ids);
                    $products = $requete->from('produits')
                        ->where("(id IN ($d))")
                        ->fetchAll();
                }
                ?>
                <?php foreach ($products as $pr): ?>
                    <tr>
                        <td>
                            <img src="assets/images/produits/<?= $pr["photo"]; ?>" width="50" height="40">
                        </td>
                        <td><?= $pr["nom"]; ?><br/>
                            <form method="post">
                                <a href="index.php?p=panier&action=supprimer&id_prod=<?= $pr["id"]; ?>">supprimer</a>
                            </form>
                        </td>
                        <td>
                            <input type="hidden" name="id_prod[<?= $pr["id"]; ?>]" value="<?= $pr["id"]; ?>"/>
                            <input type="number" value="1" name="qte[<?= $pr["id"]; ?>]">
                        </td>
                        <td><?= $pr["prix_vente"]; ?> F CFA</td>
                    </tr>
                    <?php $total_price += ($pr["prix_vente"] * $pr["quantite"]); ?>
                <?php endforeach; ?>
                <!--
                <tr>
                    <td colspan="5" align="right">
                        <strong>TOTAL: <?= $total_price . " F CFA" ?></strong>
                    </td>
                </tr>-->
                </tbody>
            </table>

            <div class="row">
                <div class="col-12">
                    <button type="submit" class="btn btn-success" name="valider">commander</button>
                    <button class="btn " type="reset">vider le panier</button>
                </div>
            </div>

        </form>
    <?php else: ?>
        <h3>Votre panier est vide!</h3>
    <?php endif; ?>
</div>

