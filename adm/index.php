<?php
session_start();
if (!isset($_SESSION['admin']) || empty($_SESSION['admin'])) {
    header('location: connexion.php');
    exit();
}
require "../database.php";


//mise à jour  lorsque l'utilisateur se connecte
if (isset($_SESSION['admin']) && !empty($_SESSION['admin'])) {
    $date = date("d-m-Y") . ' à ' . date("H:i");
    $set = array('derniere_co' => $date);
    $requete->update('utilisateurs', $set, $_SESSION['admin']['id'])->execute();
    // var_dump($date); die();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Admin G-STOCK</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="../vendor/iconfonts/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="../vendor/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="../vendor/css/vendor.bundle.addons.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <!-- plugin css for this page -->
    <link rel="stylesheet" href="../vendor/iconfonts/font-awesome/css/font-awesome.css">
    <script src="../assets/js/jquery.min.js"></script>
    <!-- endinject -->
    <link rel="shortcut icon" href="../assets/images/favicon.png"/>
</head>
<body>
<div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
        <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
            <a class="navbar-brand brand-logo" href="index.php">
                <img src="../assets/images/logo.png" alt="logo"/>
            </a>
            <a class="navbar-brand brand-logo-mini" href="index.php">
                <img src="../assets/images/logo-mini.svg" alt="logo"/>
            </a>
        </div>
        <div class="navbar-menu-wrapper d-flex align-items-center">
            <ul class="navbar-nav navbar-nav-right">
                <li class="nav-item dropdown d-none d-xl-inline-block">
                    <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown"
                       aria-expanded="false">
                        <span class="profile-text">Bienvenue, <?= $_SESSION['admin']['nom'] . " " . $_SESSION['admin']['prenom']; ?></span>
                        <img class="img-xs rounded-circle"
                             src="../assets/images/faces/<?= $_SESSION['admin']['img']; ?>" alt="Profile image">
                    </a>
                    <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
                        <a class="dropdown-item p-0">
                            <div class="d-flex border-bottom">
                                <div class="py-3 px-4 d-flex align-items-center justify-content-center">
                                    <i class="mdi mdi-bookmark-plus-outline mr-0 text-gray"></i>
                                </div>
                                <div class="py-3 px-4 d-flex align-items-center justify-content-center border-left border-right">
                                    <i class="mdi mdi-account-outline mr-0 text-gray"></i>
                                </div>
                                <div class="py-3 px-4 d-flex align-items-center justify-content-center">
                                    <i class="mdi mdi-alarm-check mr-0 text-gray"></i>
                                </div>
                            </div>
                        </a>
                        <a href="index.php?page=gerer-compte" class="dropdown-item mt-2">
                            Gerer les comptes
                        </a>
                        <a href="index.php?page=changer-info" class="dropdown-item">
                            Changer mes informations
                        </a>
                        <a href="index.php?page=changer-motdepasse" class="dropdown-item">
                            Changer mon mot de passe
                        </a>
                        <a href="deconnexion.php" class="dropdown-item">
                            Deconnexion
                        </a>
                    </div>
                </li>
            </ul>
            <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button"
                    data-toggle="offcanvas">
                <span class="mdi mdi-menu"></span>
            </button>
        </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
        <nav class="sidebar sidebar-offcanvas" id="sidebar">
            <ul class="nav">
                <li class="nav-item nav-profile">
                    <div class="nav-link">
                        <div class="user-wrapper">
                            <div class="profile-image">
                                <img src="../assets/images/faces/<?= $_SESSION['admin']['img']; ?>" alt="profile image">
                            </div>
                            <div class="text-wrapper">
                                <p class="profile-name"><?= $_SESSION['admin']['nom'] . " " . $_SESSION['admin']['prenom'] ?></p>
                                <div>
                                    <small class="designation text-muted"><?= $_SESSION['admin']['role']; ?></small>
                                    <span class="status-indicator online"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="index.php">
                        <i class="menu-icon mdi mdi-television"></i>
                        <span class="menu-title">Tableau de Bord</span>
                    </a>
                </li>


                <li class="nav-item">
                    <a class="nav-link" data-toggle="collapse" href="#ui-commande" aria-expanded="false"
                       aria-controls="ui-basic">
                        <i class="menu-icon mdi mdi-content-copy"></i>
                        <span class="menu-title">Commandes</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="collapse" id="ui-commande">
                        <ul class="nav flex-column sub-menu">
                            <li class="nav-item">
                                <a class="nav-link" href="index.php?page=ajouter-commande">Enregistrer</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php?page=liste-commande">Liste des commandes</a>
                            </li>
                        </ul>
                    </div>
                </li>


                <li class="nav-item">
                    <a class="nav-link" data-toggle="collapse" href="#ui-client" aria-expanded="false"
                       aria-controls="ui-basic">
                        <i class="menu-icon mdi mdi-content-copy"></i>
                        <span class="menu-title">Clients</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="collapse" id="ui-client">
                        <ul class="nav flex-column sub-menu">
                            <li class="nav-item">
                                <a class="nav-link" href="index.php?page=ajouter-client">Enregistrer un client</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php?page=liste-client">Liste des clients</a>
                            </li>
                        </ul>
                    </div>
                </li>
                <?php if ($_SESSION['admin']['role'] == 'admin'): ?>

                    <li class="nav-item">
                        <a class="nav-link" data-toggle="collapse" href="#ui-produit" aria-expanded="false"
                           aria-controls="ui-basic">
                            <i class="menu-icon mdi mdi-content-copy"></i>
                            <span class="menu-title">Produits</span>
                            <i class="menu-arrow"></i>
                        </a>
                        <div class="collapse" id="ui-produit">
                            <ul class="nav flex-column sub-menu">
                                <li class="nav-item">
                                    <a class="nav-link" href="index.php?page=ajouter-produit">Nouveau produit</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="index.php?page=liste-produit">Liste des produits</a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="index.php?page=marque" aria-expanded="false"
                           aria-controls="ui-basic">
                            <i class="menu-icon mdi mdi-content-copy"></i>
                            <span class="menu-title">Marques</span>
                        </a>
                    </li>


                    <li class="nav-item">
                        <a class="nav-link" href="index.php?page=categorie">
                            <i class="menu-icon mdi mdi-table"></i>
                            <span class="menu-title">Categories</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" data-toggle="collapse" href="#ui-fournisseurs" aria-expanded="false"
                           aria-controls="ui-basic">
                            <i class="menu-icon mdi mdi-content-copy"></i>
                            <span class="menu-title">Fournisseurs</span>
                            <i class="menu-arrow"></i>
                        </a>
                        <div class="collapse" id="ui-fournisseurs">
                            <ul class="nav flex-column sub-menu">
                                <li class="nav-item">
                                    <a class="nav-link" href="index.php?page=ajouter-fournisseur">Nouveau
                                        fournisseur</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="index.php?page=liste-fournisseur">Liste des
                                        fournisseurs</a>
                                </li>
                            </ul>
                        </div>
                    </li>


                    <li class="nav-item">
                        <a class="nav-link" data-toggle="collapse" href="#ui-stocks" aria-expanded="false"
                           aria-controls="ui-basic">
                            <i class="menu-icon mdi mdi-content-copy"></i>
                            <span class="menu-title">Stocks</span>
                            <i class="menu-arrow"></i>
                        </a>
                        <div class="collapse" id="ui-stocks">
                            <ul class="nav flex-column sub-menu">
                                <li class="nav-item">
                                    <a class="nav-link" href="index.php?page=ajouter_stock">Nouvelle entrée</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="index.php?page=historique">Historique</a>
                                </li>
                            </ul>
                        </div>
                    </li>

                <?php endif; ?>
            </ul>
        </nav>
        <!-- partial -->

        <div class="main-panel">
            <div class="content-wrapper">
                <?php
                if (isset($_GET['page'])) {
                    $p = $_GET['page'];
                } else {
                    $p = "";
                }

                switch ($p) {
                    case 'ajouter-client':
                        include "client/ajouter.php";
                        break;

                    case 'ajouter-fournisseur':
                        include "fournisseur/ajouter.php";
                        break;

                    case 'ajouter-produit':
                        include "produit/ajouter.php";
                        break;

                    case 'ajouter-commande':
                        include "commande/ajouter.php";
                        break;

                    case 'ajouter_stock':
                        include "stock/ajouter.php";
                        break;

                    case 'categorie':
                        include "categorie/liste.php";
                        break;

                    case 'marque':
                        include "marque/liste.php";
                        break;


                    case 'liste-produit':
                        include "produit/liste.php";
                        break;

                    case 'liste-client':
                        include "client/liste.php";
                        break;

                    case 'liste-fournisseur':
                        include "fournisseur/liste.php";
                        break;

                    case 'liste-commande':
                        include "commande/liste.php";
                        break;

                    case 'historique':
                        include "stock/liste.php";
                        break;

                    case 'changer-info':
                        include "changer_information.php";
                        break;

                    case 'changer-motdepasse':
                        include "changer_motdepasse.php";
                        break;

                    case 'gerer-compte':
                        include "compte.php";
                        break;


                    default:
                        include "accueil.php";
                }
                ?>
            </div>
            <!-- content-wrapper ends -->
            <!-- partial:partials/_footer.html -->
            <footer class="footer">

            </footer>
            <!-- partial -->
        </div>
        <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->

<!-- plugins:js -->
<script src="../vendor/js/vendor.bundle.base.js"></script>
<script src="../vendor/js/vendor.bundle.addons.js"></script>
<script src="../assets/js/off-canvas.js"></script>
<script src="../assets/js/misc.js"></script>
<script src="../assets/js/dashboard.js"></script>
</body>
</html>