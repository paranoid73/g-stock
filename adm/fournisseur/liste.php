<?php
//pour afficher la listes des clients
$donnees=$requete->from('fournisseurs');
//modification  d'un clients

if(isset($_POST['modifier'])){
    extract($_POST);
    $set = array('nom' => $nom,  'tel' => $tel, 'email' => $email);
    $requete ->update('fournisseurs', $set, $_GET['id'])->execute();
}
//Annuler la modification
if(isset($_POST['annuler'])){
    unset($_GET['id']);
}
//pour un nouveau select dans la base de donnees selon le nom sur lequel on a cliqué
if(isset($_GET['id'])){
    $valeur = $requete->from('fournisseurs')->where('id',$_GET['id'])->fetch();
}
?>


<div class="col-12">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <table class="table table-striped">
                    <tr>
                        <td>ID</td>
                        <td>Nom</td>
                        <td>Telephone</td>
                        <td>Email</td>
                        <td></td>
                    </tr>

                    <?php foreach ($donnees as $row): ?>
                        <tr>
                            <td>#<?php echo $row['id'];?></td>
                            <td> <?php echo '<a href="index.php?page=liste-fournisseur&id='.$row['id'].'">'.$row['nom'].'</a>';?></td>
                            <td> <?php echo $row['tel'];?></td>
                            <td> <?php echo $row['email'];?></td>
                            <td><a class="supprimer" href="#" id="<?= $row['id']; ?>"><i class="fa fa-trash-o fa-2x"></i></a></td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
            <hr>
            <div class="row">
                <?php if(isset($_GET['id'])): ?>
                    <div class="col-6">
                        <h3>Modification d'un fournisseur</h3>
                        <form class="forms-sample"  method="post">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nom:</label>
                                <input class="form-control" type="text" name="nom" required value="<?= $valeur['nom'] ?>">
                            </div>

                            <div class="form-group">
                                <label for="">Telephone:</label>
                                <input class="form-control" type="text" name="tel" required value="<?= $valeur['tel'] ?>">
                            </div>
                            <div class="form-group">
                                <label for="">Email:</label>
                                <input class="form-control" type="text" name="email" required value="<?= $valeur['email'] ?>">
                            </div>
                            <button type="submit" class="btn btn-success mr-2" name="modifier">Modifier</button>
                            <button class="btn btn-light" type="submit" name="annuler">Annuler</button>
                        </form>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('.supprimer').click(function(){
            var answer = confirm('Êtes vous sure de vouloir supprimer certaine ligne ?');
            if (answer)
            {
                var del_id= $(this).attr('id');
                var $ele = $(this).parent().parent();
                $.ajax({
                    type:'POST',
                    url:'fournisseur/delete.php',
                    data:{id: del_id },
                    success: function(data){
                        if(data=="YES"){
                            $ele.fadeOut().remove();
                        }else{
                            alert("Impossible de supprimer cette ligne.");
                        }
                    }
                });
            }
        })
    });
</script>