<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    extract($_POST);
    $tableau = array('nom' => $nom, 'email' => $adresse_email, 'tel' => $telephone);
    $requete->insertInto('fournisseurs', $tableau)->execute();
}
?>

<div class="card">
    <div class="card-body">
        <div class="card-description">
            Ajouter un fournisseur
        </div>
        <form method="post">
            <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <label>Nom</label>
                        <input type="text" name="nom" class="form-control" maxlength="100" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="adresse_email" class="form-control" maxlength="100" required>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label>Téléphone</label>
                        <input type="text" name="telephone" class="form-control" maxlength="8" required>
                    </div>
                </div>
            </div>
            <input type="submit" name="enregistrer" class="btn btn-success">
            <input type="reset" name="annuler" class="btn ">
        </form>
    </div>
</div>