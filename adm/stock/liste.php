<?php
$message = [];
/* récuperer les informations  */
$donnee = $requete->from('approvisionnement ap')
    ->innerJoin('produits p ON p.id = ap.id_prod')
    ->innerJoin('fournisseurs f ON f.id = ap.id_four')
    ->select('ap.id as id_ap , f.nom as nom_four, ap.quantite as quantite, p.nom as nom_produit')->fetchAll();




?>

<div class="col-12">
    <div class="card">
        <div class="card-body">
            <?php if(isset($message) && !empty($message)): ?>
                <?php foreach ($message as $key => $value): ?>
                    <div class="bg-<?= $key ?>">
                        <p class="text-white p-2"><?= $message[$key] ?></p>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>

            <div class="card-description">
                Historique des approvisionnements
            </div>
            <div class="row">
                <table class="table table-striped table-responsive">
                    <tr>
                        <td>#ID</td>
                        <td>Nom du produit</td>
                        <td>fournisseur</td>
                        <td>quantité</td>
                        <td>Date</td>
                    </tr>
                    <?php foreach ($donnee as $row): ?>
                        <tr>
                            <td><?= $row['id_ap']; ?></td>
                            <td><?= $row['nom_produit']; ?></td>
                            <td> <?= $row['nom_four']; ?></td>
                            <td> <?= $row['quantite']; ?></td>
                            <td> <?= $row['date_ap']; ?></td>

                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
            <hr>
            <div class="row">
                <?php if (isset($_GET['id'])): ?>

                    <div class="col-6">
                        <h3>Modification de l'approvisionnement </h3>
                        <form class="forms-sample" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nom</label>
                                <input class="form-control" type="text" name="nom" required
                                       value="<?php echo $valeur['nom'] ?>"><br>
                            </div>
                            <div class="form-group">
                                <label for="">Prix de Vente:</label>
                                <input class="form-control" type="text" name="prix_v" required
                                       value="<?php echo $valeur['prix_vente'] ?>"><br>
                            </div>
                            <div class="form-group">
                                <label for="">Prix d'achat:</label>
                                <input class="form-control" type="text" name="prix_a" required
                                       value="<?php echo $valeur['prix_achat'] ?>"><br>
                            </div>

                            <div class="form-group">
                                <label for="categorie">Catégorie:</label>
                                <select class="form-control" name="categ">
                                    <?php foreach ($cate as $key => $value): ?>
                                        <?php if($valeur['id_categorie'] == $key ): ?>
                                            <option value="<?= $key ?>" selected="selected"><?= $value ?></option>
                                        <?php else: ?>
                                            <option value="<?= $key ?>"><?= $value ?></option>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="categorie">Marque:</label>
                                <select class="form-control" name="marque">
                                    <?php foreach ($ma as $ke => $val): ?>
                                        <?php if($valeur['id_marque'] == $key ): ?>
                                            <option value="<?= $ke ?>"  selected="selected"><?= $val ?></option>
                                        <?php else: ?>
                                            <option value="<?= $key ?>"><?= $value ?></option>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Modifier l'image du produit:</label>
                                <div class="input-group">
                                    <input type="file" class="form-control file-upload-info" name="img">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="">Description:</label>
                                <textarea class="form-control" name="desc"
                                          required><?php echo $valeur['description'] ?></textarea><br>
                            </div>

                            <button type="submit" class="btn btn-success mr-2" name="modifier">Modifier</button>
                            <button class="btn btn-light" type="submit" name="annuler">Annuler</button>
                        </form>
                    </div>
                    <div class="col-6">
                        <img class="pt-5" src="../assets/images/produits/<?php echo $valeur['photo'] ?>" alt="">
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
