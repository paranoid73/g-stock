<?php
$fournisseurs = $requete->from('fournisseurs');
/*lorsqu'on fait une réquête POST*/
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    /*Importe les variables dans la table des symboles*/
    extract($_POST);
    /*enregistrer les informations dans la base de donnée*/
    $tableau = array('id_prod' => $id_produit,'id_four' => $id_four, 'quantite'=> (int)$quantite);
    $reponse = (bool) $requete->insertInto('approvisionnement', $tableau)->execute();
    /* modifier la quantité dans la table produit*/
    $field = 'quantite';
    $amount = $quantite;
    $id = (int) $id_produit;
    $increment = array($field => new \Envms\FluentPDO\Literal($field.' + '.$amount));
    $miseajour = $requete->update('produits')->set($increment)->where('id',$id)->execute();
    $message = [];
    if($reponse){
        $message['success'] = "Le produit a  été ajouté dans la mise à jour";
    }else{
        $message['danger'] = "Erreur pendant l'insertion  dans la base de donnée";
    }
}
?>

<div class="col-12">
    <div class="card">
        <div class="card-body">
            <?php if(isset($message) && !empty($message)): ?>
                <?php foreach ($message as $key => $value): ?>
                    <div class="bg-<?= $key ?>">
                        <p class="text-white p-2"><?= $message[$key] ?></p>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>

            <p class="card-description">
                Approvisionner la boutique
            </p>
            <form class="forms-sample" method="post">
                <div class="form-group">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="">Rechercher le produit</label>
                                <input class="form-control" id="recherche" type="text" name="nom" placeholder="Nom du produit" maxlength="50">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="">Produit selectionné</label>
                                <select class="form-control" id="select-produit" name="id_produit">
                                    <option value="0">Selectionner</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="">Fournisseur</label>
                            <select class="form-control" name="id_four">
                                <?php foreach ($fournisseurs as $four): ?>
                                    <option value="<?= $four['id'] ?>"><?= $four['nom'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Quantité :</label>
                            <input class="form-control" type="number" name="quantite" required  maxlength="100">
                        </div>
                    </div>
                </div>


                <button type="submit" class="btn btn-success mr-2" name="valider">Enregistrer</button>
                <button type='reset' class="btn btn-light" >Annuler</button>
            </form>
        </div>
    </div>
</div>



<script>
    $(document).ready(function() {
        $('#recherche').on('blur keyup',function () {
            var nom = $(this).val();
            $.ajax({
                type:'POST',
                url:'models.php',
                data:{rech_prod:nom},
                dataType: 'json',
                success:function (response) {
                    var len = response.length;
                    $("#select-produit").empty();
                    for( var i = 0; i<len; i++){
                        var id = response[i]['id'];
                        var name = response[i]['nom'];
                        $("#select-produit").append("<option value='"+id+"'>"+name+"</option>");
                    }

                }
            });
        });

    });
</script>