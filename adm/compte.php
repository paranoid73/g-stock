<?php
$roles = $requete->from('type_utilisateur')->fetchPairs('id', 'nom');

    //récupérer la liste des comptes
    $donnees = $requete->from('utilisateurs u')
    ->leftJoin('type_utilisateur t ON u.id_type_utilisateur = t.id')
    ->select('t.nom as role_user ');


    //insertion dans la base de données
    if (isset($_POST['valider'])) {
        extract($_POST);

        //repertoire de l'image
    $target_dir = "../assets/images/faces/";
    $target_file = $target_dir . basename($_FILES["img"]["name"]);
    move_uploaded_file($_FILES["img"]["tmp_name"], $target_file);

        $tableau = array('nom' => $nom,'prenom'=> $prenom , 'email'=> $email,'motdepasse'=> $motdepasse, 'id_type_utilisateur'=> $role, 'img'=> $_FILES['img']['name']);
    $requete->insertInto('utilisateurs', $tableau)->execute();

    }

?>

<div class="col-12">
    <div class="card">
        <div class="card-body">
            <div class="card-description">
               <h4> Créer un compte </h4>
            </div>
            <form class="forms-sample" method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nom</label>
                            <input class="form-control" type="text" name="nom" required autofocus>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Prenom</label>
                            <input class="form-control" type="text" name="prenom" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Email</label>
                            <input class="form-control" type="text" name="email" required>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="form-group">
                            <label for="exampleInputPassword1">mot de passe</label>
                            <input class="form-control" type="password" name="motdepasse" required><br>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >Rôle</label>
                            <select class="form-control" name="role">
                                <?php foreach ($roles as $key => $value): ?>
                                    <option value="<?= $key ?>"><?= $value ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>selectionner une image</label>
                            <div class="input-group">
                                <input type="file" name="img" class="form-control file-upload-info" required>
                            </div>
                        </div>
                    </div>
                </div>


                <button type="submit" class="btn btn-success mr-2" name="valider">Créer le compte</button>
                <button class="btn btn-light" type="reset">Annuler</button>
            </form>
        </div>
    </div>
</div>

<div class="col-12">
    <div class="card">
        <div class="card-body">
            <div class="card-description">
               <h4> Liste des comptes</h4> 
            </div>
            <div class="row">
                <table class="table table-striped table-responsive">
                    <tr>
                        <td>Email</td>
                        <td>Nom</td>
                        <td>Prenoms</td>
                        <td>Role</td>
                        <td>Photo</td>
                        <td>Dernière connexion</td>
                    </tr>

                    <?php
                    foreach ($donnees as $row) {

                        ?>
                        <tr>
                            <td> <?php echo $row['email'];?></td>
                            <td> <?php echo $row['nom'];?></td>
                            <td> <?php echo $row['prenom'];?></td>
                            <td> <?php echo $row['role_user'];?></td>
                            <td> <img src="../assets/images/faces/<?php echo $row['img'];?>" alt=""></td>
                             <td> <?php echo $row['derniere_co'];?></td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
</div>

