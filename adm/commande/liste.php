<?php
$message = [];
/* récuperer les informations  */
$donnees = $requete->from('commandes c')
    ->innerJoin('clients cl ON cl.id = c.id_client')
    ->innerJoin('produits p ON p.id = c.id_prod')
    ->select('p.id as id_prod , c.id as id_cmd , c.quantite as quantite , p.photo as photo,  p.nom as nom_produit , cl.nom as nom_client , cl.prenom as prenom_client , c.date as date_achat')
->fetchAll();



/* Annuler la modification */
if (isset($_POST['annuler'])) {
    unset($_GET['id']);
}

/* pour un nouveau select dans la base de donnees selon le nom sur lequel on a cliqué */
if (isset($_GET['id'])) {
    $valeur = $requete->from('produits')->where('id', $_GET['id'])->fetch();
}

if (isset($_GET['id_cmd'])) {
    $set = array('confirme' => 1);
    $valeur = $requete->update('commandes',$set)->where('id', $_GET['id_cmd'])->execute();
    /* modifier la quantité dans la table produit*/
    $field = 'quantite';
    $amount = $_GET['qte'];
    $id = (int) $_GET['id_prod'];
    $increment = array($field => new \Envms\FluentPDO\Literal($field.' - '.$amount));
    $miseajour = $requete->update('produits')->set($increment)->where('id',$id)->execute();
}

?>

<div class="col-12">
    <div class="card">
        <div class="card-body">
            <?php if (isset($message) && !empty($message)): ?>
                <?php foreach ($message as $key => $value): ?>
                    <div class="bg-<?= $key ?>">
                        <p class="text-white p-2"><?= $message[$key] ?></p>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>

            <div class="card-description">
                Listes des commandes
            </div>
            <div class="row">
                <form method="post">
                    <table class="table table-striped  table-bordered table-responsive">
                        <tr>
                            <td></td>
                            <td>#ID</td>
                            <td>Nom du produit</td>
                            <td>Client</td>
                            <td>quantité</td>
                            <td>Photo</td>
                            <td>Date</td>
                            <td></td>
                        </tr>

                        <?php foreach ($donnees as $row): ?>
                            <tr>
                                <?php if ($row['confirme'] == 1): ?>
                                    <td></td>
                                <?php else: ?>
                                    <td><a href="index.php?page=liste-commande&id_cmd=<?= $row['id_cmd'] ?>&qte=<?= $row['quantite'] ?>&id_prod=<?= $row['id_prod'] ?>">confirmé</a></td>
                                <?php endif; ?>
                                <td># <?php echo $row['id_cmd']; ?></td>
                                <td> <?php echo $row['nom_produit']; ?></td>
                                <td> <?php echo $row['nom_client']; ?></td>
                                <td> <?php echo $row['quantite']; ?></td>
                                <td><img src=" ../assets/images/produits/<?php echo $row['photo']; ?>" alt="">
                                </td>
                                <td> <?php echo $row['date_achat']; ?></td>
                                <?php if ($row['confirme'] == 1): ?>
                                    <td><label class="badge badge-success">confirmé</label></td>
                                <?php else: ?>
                                    <td><label class="badge badge-danger">en attente</label></td>
                                <?php endif; ?>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </form>
            </div>
            <hr>
        </div>
    </div>
</div>