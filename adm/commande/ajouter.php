<?php
$cate = $requete->from('categories')->fetchPairs('id', 'nom');
$ma = $requete->from('marques')->fetchPairs('id', 'nom');

/*lorsqu'on fait une réquête POST*/
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    /*Importe les variables dans la table des symboles*/
    extract($_POST);

    /*enregistrer les informations dans la base de donnée*/
    $tableau = array('id_client' => $id_client,'id_prod'=> $id_produit , 'quantite'=> (int)$quantite,'confirme'=> 1);
    $reponse = (bool) $requete->insertInto('commandes', $tableau)->execute();

    /* modifier la quantité dans la table produit*/
    $field = 'quantite';
    $amount = $quantite;
    $id = (int) $id_produit;
    $increment = array($field => new \Envms\FluentPDO\Literal($field.' - '.$amount));
    $miseajour = $requete->update('produits')->set($increment)->where('id',$id)->execute();

    /* message d'erreur */
    $message = [];
    if($reponse){
        $message['success'] = "commande enregistré ";
    }else{
        $message['danger'] = "Erreur pendant l'insertion dans la base de donnée";
    }
}

?>

<div class="col-12">
    <div class="card">
        <div class="card-body">
            <?php if(isset($message) && !empty($message)): ?>
                <?php foreach ($message as $key => $value): ?>
                    <div class="bg-<?= $key ?>">
                        <p class="text-white p-2"><?= $message[$key] ?></p>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>

            <p class="card-description">
               Enregistrer une nouvelle commande
            </p>
            <form class="forms-sample" method="post">
                <div class="form-group">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="">Rechercher le client</label>
                                <input class="form-control" id="recherche" type="text" name="nom" placeholder="Nom du client" required maxlength="50">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for=""></label>
                                <select class="form-control" id="select-client" name="id_client">
                                        <option value="0">Selectionner</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="" >Catégorie</label>
                            <select class="form-control" name="categorie" id="select-categorie">
                                <?php foreach ($cate as $key => $value): ?>
                                    <option value="<?= $key ?>"><?= $value ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="form-group">
                            <label for="" >Marque</label>
                            <select class="form-control" name="marque" id="select-marque">
                                <?php foreach ($ma as $ke => $value): ?>
                                    <option value="<?= $ke ?>"><?= $value ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <div class="form-group">
                        <label for="" >Produit</label>
                        <select class="form-control" name="id_produit" id="produit">
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">Quantité :</label>
                            <input class="form-control" type="number" name="quantite" required value="1">
                        </div>
                    </div>
                </div>


                <button type="submit" class="btn btn-success mr-2" name="valider">Enregistrer</button>
                <button type='reset' class="btn btn-light" >Annuler</button>
            </form>
        </div>
    </div>
</div>



<script>
    $(document).ready(function() {
        $('#recherche').on('blur keyup',function () {
            var nom = $(this).val();
            $.ajax({
                type:'POST',
                url:'models.php',
                data:{recherche_nom:nom},
                dataType: 'json',
                success:function (response) {
                    var len = response.length;
                    $("#select-client").empty();
                    for( var i = 0; i<len; i++){
                        var id = response[i]['id'];
                        var name = response[i]['nom'];
                        $("#select-client").append("<option value='"+id+"'>"+name+"</option>");
                    }

                }
            });
        });


        $("#select-categorie").change(function(){
            var marque = $( "#select-marque" ).val();
            var categorie = $("#select-categorie" ).val();
            $.ajax({
                type:'POST',
                url:'models.php',
                data:{id_marque:marque,id_categorie:categorie},
                dataType: 'json',
                success:function (response) {
                    var len = response.length;
                    $("#produit").empty();
                    for( var i = 0; i<len; i++){
                        var id = response[i]['id'];
                        var name = response[i]['nom'];
                        $("#produit").append("<option value='"+id+"'>"+name+"</option>");
                    }
                }
            });
        }).trigger('change');;

        $("#select-marque").change(function(){
           var marque = $( "#select-marque" ).val();
           var categorie = $("#select-categorie" ).val();
            $.ajax({
                type:'POST',
                url:'models.php',
                data:{id_marque:marque,id_categorie:categorie},
                dataType: 'json',
                success:function (response) {
                    var len = response.length;
                    $("#produit").empty();
                    for( var i = 0; i<len; i++){
                        var id = response[i]['id'];
                        var name = response[i]['nom'];
                        $("#produit").append("<option value='"+id+"'>"+name+"</option>");
                    }
                }
            });
        });
    });
</script>