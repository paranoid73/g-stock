<?php
require "../database.php";

extract($_POST);
$tableau = [];

/* si on reçoit une réquête POST pour le client */
if(isset($recherche_nom)){
    $clients = $requete->from('clients')->where('(nom LIKE ?)', '%' . $recherche_nom . '%')->fetchAll();
    if(count($clients)>0){
        foreach ($clients as $cl) {
            $id = $cl['id'];
            $nom = $cl['nom'] . ' ' . $cl['prenom'];
            $tableau[] = ['id' => $id, 'nom' => $nom];
        }
    }
}

/* si on reçoit une réquête POST pour le client */
if(isset($rech_prod)){
    $resultat = $requete->from('produits')->where('(nom LIKE ?)', '%' . $rech_prod . '%')->fetchAll();
    if(count($resultat)>0){
        foreach ($resultat as $cl) {
            $id = $cl['id'];
            $nom = $cl['nom'];
            $tableau[] = ['id' => $id, 'nom' => $nom];
        }
    }
}

/* si on recçoit une réquête POST pour l'id marque et categorie*/
if(isset($id_marque) && isset($id_categorie)){

    $produits = $requete->from('produits')->where('id_marque',(int) $id_marque)->where('id_categorie',(int)$id_categorie)->fetchAll();
    if(count($produits)>0){
        foreach ($produits as $cl) {
            $id = $cl['id'];
            $nom = $cl['nom'];
            $tableau[] = ['id' => $id, 'nom' => $nom];
        }
    }
}

// encoding array to json format
echo json_encode($tableau);