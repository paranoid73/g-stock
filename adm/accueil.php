<?php
$clients    = $requete->from('clients')->count();
$commandes  = $requete->from('commandes')->count();
$rayons     = $requete->from('categories')->count();

?>

<div class="row">
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
            <div class="card-body">
                <div class="clearfix">
                    <div class="float-left">
                        <i class="mdi mdi-cube text-danger icon-lg"></i>
                    </div>
                    <div class="float-right">
                        <p class="mb-0 text-right">Total Revenue</p>
                        <div class="fluid-container">
                            <h3 class="font-weight-medium text-right mb-0">0</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
            <div class="card-body">
                <div class="clearfix">
                    <div class="float-left">
                        <i class="mdi mdi-receipt text-warning icon-lg"></i>
                    </div>
                    <div class="float-right">
                        <p class="mb-0 text-right">Nombre de Rayons</p>
                        <div class="fluid-container">
                            <h3 class="font-weight-medium text-right mb-0"><?= $rayons ?></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
            <div class="card-body">
                <div class="clearfix">
                    <div class="float-left">
                        <i class="mdi mdi-poll-box text-success icon-lg"></i>
                    </div>
                    <div class="float-right">
                        <p class="mb-0 text-right">Total des ventes</p>
                        <div class="fluid-container">
                            <h3 class="font-weight-medium text-right mb-0"><?= $commandes ?></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
            <div class="card-body">
                <div class="clearfix">
                    <div class="float-left">
                        <i class="mdi mdi-account-location text-info icon-lg"></i>
                    </div>
                    <div class="float-right">
                        <p class="mb-0 text-right">Clients</p>
                        <div class="fluid-container">
                            <h3 class="font-weight-medium text-right mb-0"><?= $clients ?></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


