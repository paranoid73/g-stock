<?php
$marques = $requete->from('categories');

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $message = [];
    if (isset($_POST['valider'])) {
        extract($_POST);
        if (!empty($nom)) {
            $tableau = array('nom' => $nom);
        }
        $reponse = (bool)$requete->insertInto('categories', $tableau)->execute();
        if ($reponse) {
            $message['success'] = "la categorie ajouté dans la base de donnée";
        } else {
            $message['danger'] = "Erreur pendant l'insertion  dans la base de donnée";
        }
    }

    /*modification marque*/
    if (isset($_POST['modifier'])) {
        extract($_POST);
        $set = array('nom' => $nom);
        $reponse = (bool)$requete->update('categories', $set, $_GET['id'])->execute();
        if ($reponse) {
            $message['success'] = "la categorie vient d'être modifiée dans la base de donnée";
        } else {
            $message['danger'] = "Erreur pendant la modification dans la base de donnée";
        }
    }
    /*action annuler*/
    if (isset($_POST['annuler'])) {
        unset($_GET['id']);
        header('location:index.php?page=liste-categorie');
    }

}


if (isset($_GET['id']) && !empty($_GET['id'])) {
    $valeur = $requete->from('categories')->where('id', $_GET['id'])->fetch();
}


?>

<div class="col-12">
    <div class="card">
        <div class="row">
            <div class="col-6">
                <div class="card-body">
                    <?php if (isset($message) && !empty($message)): ?>
                        <?php foreach ($message as $key => $value): ?>
                            <div class="bg-<?= $key ?>">
                                <p class="text-white p-2"><?= $message[$key] ?></p>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <div class="card-description">
                        Ajouter une nouvelle categorie
                    </div>
                    <form class="forms-sample" method="post">
                        <div class="form-group">
                            <label for="">Nom de la categorie</label>
                            <input class="form-control" type="text" name="nom" placeholder="Nom de la categorie">
                        </div>
                        <input type="submit" class="btn btn-success mr-2" name="valider">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<hr>

<div class="card">
    <div class="card-body">
        <div class="card-description">
            Liste des categories
        </div>
        <div class="row">
            <div class="col-6">
                <table class="table table-responsive">
                    <tr>
                        <td>ID</td>
                        <td>Nom</td>
                        <td></td>
                    </tr>
                    <?php foreach ($marques as $marq): ?>
                        <tr>
                            <td> <?= $marq['id'] ?></td>
                            <td><a href="index.php?page=categorie&id=<?=$marq['id'] ?>"><?= $marq['nom'] ?></a></td>
                            <td><a class="supprimer" href="#" id="<?= $marq['id']; ?>"><i class="fa fa-trash-o fa-2x"></i></a></td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
            <div class="col-6">
                <?php if (isset($_GET['id']) && !empty($_GET['id'])): ?>
                    <div>
                        <br>
                        <form method="post">
                            <div class="form-group">
                                <label for="">Nom:</label>
                                <input class="form-control" type="text" name="nom" required
                                       value="<?= $valeur['nom'] ?>">
                            </div>
                            <button type="submit" class="btn btn-success mr-2" name="modifier">Modifier</button>
                            <button type="submit" class="btn" name="annuler">Annuler</button>
                        </form>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<hr>

<script>
    $(document).ready(function() {
        $('.supprimer').click(function(){
            var answer = confirm('Êtes vous sure de vouloir supprimer certaine ligne ?');
            if (answer)
            {
                var del_id= $(this).attr('id');
                var $ele = $(this).parent().parent();
                $.ajax({
                    type:'POST',
                    url:'categorie/delete.php',
                    data:{id: del_id },
                    success: function(data){
                        if(data=="YES"){
                            $ele.fadeOut().remove();
                        }else{
                            alert("Impossible de supprimer cette ligne.");
                        }
                    }
                });
            }
        })
    });
</script>