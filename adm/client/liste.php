<?php
//pour afficher la listes des clients
$donnees = $requete->from('clients');

//modification  d'un clients
if (isset($_POST['modifier'])) {
    extract($_POST);
    $set = array('nom' => $nom, 'prenom' => $prenom, 'tel' => $tel, 'email' => $email, 'etat' => $etat);
    $requete->update('clients', $set, $_GET['id'])->execute();
}

//Annuler la modification
if (isset($_POST['annuler'])) {
    unset($_GET['id']);
}

//pour un nouveau select dans la base de donnees selon le nom sur lequel on a cliqué
if (isset($_GET['id'])) {
    $valeur = $requete->from('clients')->where('id', $_GET['id'])->fetch();
}
?>


<div class="col-12">
    <div class="card">
        <div class="card-body">
            <div class="card-description">
                Liste des clients
            </div>
            <div class="row">
                <table class="table table-striped table-bordered">
                    <tr>
                        <td>Nom</td>
                        <td>Prenom</td>
                        <td>Telephone</td>
                        <td>Email</td>
                        <td></td>
                        <td></td>
                    </tr>

                    <?php
                    foreach ($donnees as $row) {
                        ?>
                        <tr>
                            <td> <?php echo '<a href="index.php?page=liste-client&amp;id=' . $row['id'] . '">' . $row['nom'] . '</a>'; ?></td>
                            <td> <?php echo $row['prenom']; ?></td>
                            <td> <?php echo $row['tel']; ?></td>
                            <td> <?php echo $row['email']; ?></td>
                            <?php if ($row['etat'] == 0): ?>
                                <td><label class="badge badge-info">desactivé</label></td>
                            <?php else: ?>
                                <td><label class="badge badge-success">activé</label></td>
                            <?php endif; ?>
                            <td><a class="supprimer" href="#" id="<?= $row['id']; ?>"><i class="fa fa-trash-o fa-2x"></i></a></td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
            <hr>
            <div class="row">
                <?php if (isset($_GET['id'])): ?>
                    <div class="col-12">
                        <h5>Modification d'un client</h5>
                        <form class="forms-sample" method="post">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Nom:</label>
                                        <input class="form-control" type="text" name="nom" required
                                               value="<?php echo $valeur['nom'] ?>"><br>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="">Prenom:</label>
                                        <input class="form-control" type="text" name="prenom" required
                                               value="<?php echo $valeur['prenom'] ?>"><br>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Telephone:</label>
                                        <input class="form-control" type="text" name="tel" required
                                               value="<?php echo $valeur['tel'] ?>"><br>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Email:</label>
                                        <input class="form-control" type="text" name="email" required
                                               value="<?php echo $valeur['email'] ?>"><br>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="">statut du compte</label>
                                        <select name="etat" class="custom-select">
                                            <?php if ($valeur['etat'] == 0): ?>
                                                <option value="0" selected="selected">desactivé ce compte</option>
                                                <option value="1">activé ce compte</option>
                                            <?php else: ?>
                                                <option value="1" selected="selected" >activé ce compte</option>
                                                <option value="0" >desactivé ce compte</option>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-success mr-2" name="modifier">Modifier</button>
                            <button class="btn btn-light" type="submit" name="annuler">Annuler</button>
                        </form>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function() {
        $('.supprimer').click(function(){
            var answer = confirm('Êtes vous sure de vouloir supprimer certaine ligne ?');
            if (answer)
            {
                var del_id= $(this).attr('id');
                var $ele = $(this).parent().parent();
                $.ajax({
                    type:'POST',
                    url:'client/delete.php',
                    data:{id: del_id },
                    success: function(data){
                        if(data=="YES"){
                            $ele.fadeOut().remove();
                        }else{
                            alert("Impossible de supprimer cette ligne.");
                        }
                    }
                });
            }
        })
    });
</script>