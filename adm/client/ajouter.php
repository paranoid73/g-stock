<?php
if(isset($_POST['valider'])){
    $message = [];
    extract($_POST);

    $tableau = array('nom' => $nom,'prenom' => $prenom,'tel' => $tel,'email' => $email,'tel' => $tel,'motdepasse' => $mdp);
    $reponse = $requete->insertInto('clients', $tableau)->execute();

    if($reponse){
        $message['success'] = "Enregistrement effectuée";
    }else{
        $message['danger'] = "Erreur pendant l'insertion dans la base de donnée";
    }

}
?>

<div class="col-12">
    <div class="card">
        <div class="card-body">
            <?php if(isset($message) && !empty($message)): ?>
                <?php foreach ($message as $key => $value): ?>
                    <div class="bg-<?= $key ?>">
                        <p class="text-white p-2"><?= $message[$key] ?></p>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
            <p class="card-description">
                Ajouter un nouveau Client
            </p>
            <form class="forms-sample" method="post">
                <div class="form-group">
                    <label for="exampleInputEmail1">Nom</label>
                    <input class="form-control" type="text" name="nom" placeholder="votre nom" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Prenom</label>
                    <input class="form-control" type="text" name="prenom" placeholder="votre Prenom" required>
                </div>


                <div class="form-group">
                    <label for="exampleInputPassword1">Telephone</label>
                    <input class="form-control" type="text" name="tel" required maxlength="8">
                </div>
                <div>
                    <label for="exampleInputPassword1">Email</label>
                    <input class="form-control" type="text" name="email" maxlength="100" required>
                </div>
                <div>
                    <label for="exampleInputPassword1">Mot de Passe</label>
                    <input class="form-control" type="password" name="mdp" required maxlength="100">
                </div>

                <button type="submit" class="btn btn-success mr-2" name="valider">Enregistrer</button>
                <button type='reset' class="btn btn-light" >Annuler</button>
            </form>
        </div>
    </div>
</div>

