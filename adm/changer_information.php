<?php

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    extract($_POST);
    $message = [];
    if (empty($nom) || empty($prenom) || empty($email)) {
        $message['danger'] = "Remplissez tous les champs";
    } else {
        $tableau = array('nom' => $nom, 'prenom' => $prenom, 'email' => $email);
        $requete->update('utilisateurs', $tableau, $_SESSION['admin']['id'])->execute();
        if ($requete) {
            $message['success'] = "Enregistrement effectué";
        }
    }
}
?>

<div class="col-12">
    <div class="card">
        <div class="card-body">
            <?php if (isset($message) && !empty($message)): ?>
                <?php foreach ($message as $key => $value): ?>
                    <p class="text-<?= $key ?> text-capitalize ">
                        <?= $value; ?>
                    </p>
                <?php endforeach; ?>
            <?php endif; ?>
            <p class="card-description">
                Changer vos informations personnelles
            </p>
            <form class="forms-sample" method="post">
                <div class="form-group">
                    <label for="exampleInputEmail1">Nom</label>
                    <input class="form-control" type="text" name="nom" value="<?= $_SESSION['admin']['nom'] ?>"
                           maxlength="100">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Prenom</label>
                    <input class="form-control" type="text" name="prenom" value="<?= $_SESSION['admin']['prenom'] ?>"
                           maxlength="100">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Email</label>
                    <input class="form-control" type="email" name="email" value="<?= $_SESSION['admin']['email'] ?>"
                           maxlength="100">
                </div>

                <button type="submit" class="btn btn-success mr-2" name="changer_motdepasse">Enregistrer</button>
                <button class="btn btn-light">Annuler</button>
            </form>
        </div>
    </div>
</div>

