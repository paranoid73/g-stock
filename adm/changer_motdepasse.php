<?php

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    extract($_POST);
    $message = [];
    if (empty($a_motdepasse) || empty($n_motdepasse) ) {
        $message['danger'] = "Remplissez tous les champs";
    } else if($a_motdepasse != $_SESSION['admin']['motdepasse']){
        $message['danger'] = "votre ancien mot de passe n'est pas correcte.";
    }
    else {
        $tableau = array('motdepasse' => $n_motdepasse);
        $requete->update('utilisateurs', $tableau, $_SESSION['admin']['id'])->execute();
        if ($requete) {
            $message['success'] = "mot de passe modifié effectué";
        }
    }
}
?>

<div class="col-12">
    <div class="card">
        <div class="card-body">
            <?php if (isset($message) && !empty($message)): ?>
                <?php foreach ($message as $key => $value): ?>
                    <p class="text-<?= $key ?> text-capitalize ">
                        <?= $value; ?>
                    </p>
                <?php endforeach; ?>
            <?php endif; ?>
            <p class="card-description">
                Changer mon mot de passe
            </p>
            <form class="forms-sample" method="post">
                <div class="form-group">
                    <label for="exampleInputEmail1">Ancien mot de passe</label>
                    <input class="form-control" type="password" name="a_motdepasse" maxlength="100">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Nouveau mot de passe</label>
                    <input class="form-control" type="password" name="n_motdepasse" maxlength="100">
                </div>

                <button type="submit" class="btn btn-success mr-2" name="changer_motdepasse">Enregistrer</button>
                <button class="btn btn-light" type="reset">Annuler</button>
            </form>
        </div>
    </div>
</div>

