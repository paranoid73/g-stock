<?php
session_start();
require "../database.php";

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    extract($_POST);

    $message = [];
    /*verification de l'adresse email */
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $message['erreur'] = "l'adresse email n'est pas valide";
    }

    if (empty($message)) {
        $reponse = $requete->from("utilisateurs u")->innerJoin('type_utilisateur t ON u.id_type_utilisateur = t.id')
                            ->where('email',$email)
                            ->where('motdepasse',$motdepasse)
                            ->select('t.nom as role')
                            ->fetch();

        if(!empty($reponse)){
            $_SESSION['admin'] = $reponse;
            header('location:index.php');
        }else{
            $message['erreur'] = "email ou mot de passe non valide , veuillez réessayer  .";
        }

    }

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>ADMIN</title>
    <link rel="stylesheet" href="../vendor/iconfonts/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="../vendor/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="../vendor/css/vendor.bundle.addons.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="shortcut icon" href="../assets/images/favicon.png"/>
</head>

<body>
<div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
        <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">
            <div class="row w-100">
                <div class="col-lg-4 mx-auto">
                    <div class="auto-form-wrapper">
                        <?php if(isset($message['erreur']) && !empty($message['erreur'])): ?>
                        <p class="text-danger text-capitalize ">
                            <?= $message['erreur']; ?>
                        </p>
                        <?php endif; ?>
                        <form action="#" method="post">
                            <div class="form-group">
                                <label class="label">Email</label>
                                <div class="input-group">
                                    <input type="email" name="email" class="form-control" placeholder="Username">
                                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="mdi mdi-check-circle-outline"></i>
                      </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="label">mot de passe</label>
                                <div class="input-group">
                                    <input type="password" name="motdepasse" class="form-control">
                                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="mdi mdi-check-circle-outline"></i>
                      </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary submit-btn btn-block">se connecter</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->
<!-- plugins:js -->
<script src="../vendor/js/vendor.bundle.base.js"></script>
<script src="../vendor/js/vendor.bundle.addons.js"></script>
<!-- endinject -->
<!-- inject:js -->
<script src="../assets/js/off-canvas.js"></script>
<script src="../assets/js/misc.js"></script>
<!-- endinject -->
</body>

</html>