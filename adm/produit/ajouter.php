<?php
$cate = $requete->from('categories')->fetchPairs('id', 'nom');
$ma = $requete->from('marques')->fetchPairs('id', 'nom');

/*lorsqu'on fait une réquête POST*/
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    /*Importe les variables dans la table des symboles*/
    extract($_POST);

    /* definition du repertoire de l'image */
    $target_dir = "../assets/images/produits/";
    $target_file = $target_dir . basename($_FILES["img"]["name"]);

    /* deplacer l'image dans vers notre serveur */
    move_uploaded_file($_FILES["img"]["tmp_name"], $target_file);

    /*enregistrer les informations dans la base de donnée*/
    $tableau = array('nom' => $nom,'prix_vente'=> $prix_v , 'prix_achat'=> $prix_a,'id_categorie'=> $categorie,
        'id_marque'=> $marque, 'photo'=> $_FILES['img']['name'],'description'=> $desc);
    $last_insert =  $requete->insertInto('produits', $tableau)->execute();
    /* message d'erreur */
    $message = [];
    if($last_insert){
        $message['success'] = "produit ajouté dans la base de donnée";
    }else{
        $message['danger'] = "Erreur pendant l'insertion du produit dans la base de donnée";
    }
}

?>

<div class="col-12">
    <div class="card">
        <div class="card-body">
            <?php if(isset($message) && !empty($message)): ?>
            <?php foreach ($message as $key => $value): ?>
                <div class="bg-<?= $key ?>">
                    <p class="text-white p-2"><?= $message[$key] ?></p>
                </div>
            <?php endforeach; ?>
            <?php endif; ?>

            <p class="card-description">
                Ajouter un nouveau produit
            </p>
            <form class="forms-sample" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="">Nom</label>
                    <input class="form-control" type="text" name="nom" placeholder="Nom du produit" required maxlength="50">
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Prix de vente</label>
                            <input class="form-control" type="text" name="prix_v" placeholder="Prix de vente" required maxlength="10">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Prix d'achat</label>
                            <input class="form-control" type="text" name="prix_a" placeholder="Prix d'achat " required maxlength="10">
                        </div>
                    </div>
                </div>
                
                    <div class="row">
                       <div class="col-6">
                           <div class="form-group">
                               <label for="" >Catégorie</label>
                               <select class="form-control" name="categorie">
                                   <?php foreach ($cate as $key => $value): ?>
                                       <option value="<?= $key ?>"><?= $value ?></option>
                                   <?php endforeach; ?>
                               </select>
                           </div>
                       </div>

                        <div class="col-6">
                            <div class="form-group">
                                <label for="" >Marque</label>
                                <select class="form-control" name="marque">
                                    <?php foreach ($ma as $ke => $value): ?>
                                        <option value="<?= $ke ?>"><?= $value ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>

                <div class="form-group">
                            <label>Image du produit</label>
                            <div class="input-group">
                                <input type="file" class="form-control file-upload-info"  name="img">
                            </div>
                </div>

                <div class="form-group">
                    <label for="exampleInputPassword1">description</label>
                    <textarea class="form-control" name="desc" placeholder="Desciption" required rows="10"></textarea>
                </div>

                <button type="submit" class="btn btn-success mr-2" name="valider">Enregistrer</button>
                <button type='reset' class="btn btn-light" >Annuler</button>
            </form>
        </div>
    </div>
</div>

