<?php
$message = [];
/* récuperer les informations  */
$donnees = $requete->from('produits p')
    ->leftJoin('categories c ON p.id_categorie = c.id')
    ->leftJoin('marques m  ON p.id_marque = m.id')
    ->select('c.nom as nom_categorie , m.nom as nom_marque');

$categories = $requete->from('categories')->fetchPairs('id', 'nom');
$marques = $requete->from('marques')->fetchPairs('id', 'nom');


/* pour une modification d'un produit sans modifier la photo */
if (isset($_POST['modifier']) && empty($_FILES["img"]["name"])) {
    extract($_POST);
    $set = array('nom' => $nom, 'prix_vente' => $prix_v, 'prix_achat' => $prix_a, 'id_marque' => $marque, 'id_categorie' => $categ, 'description' => $desc);
    $reponse = (bool)$requete->update('produits', $set, $_GET['id'])->execute();
    if ($reponse) {
        $message['success'] = "produit ajouté dans la base de donnée";
    } else {
        $message['danger'] = "Erreur pendant l'insertion du produit dans la base de donnée";
    }
}

/* pour une modification d'un produit avec  la photo */
if (isset($_POST['modifier']) && !empty($_FILES["img"]["name"])) {
    extract($_POST);
    /* repertoire de l'image */
    $target_dir = "../assets/images/produits/";
    $target_file = $target_dir . basename($_FILES["img"]["name"]);
    move_uploaded_file($_FILES['img']['tmp_name'], $target_file);

    $set = array('nom' => $nom, 'prix_vente' => $prix_v, 'prix_achat' => $prix_a, 'id_marque' => $marque, 'id_categorie' => $categ, 'description' => $desc, 'photo' => $_FILES['img']['name']);
    $reponse = (bool)$requete->update('produits', $set, $_GET['id'])->execute();
    if ($reponse) {
        $message['success'] = "produit ajouté dans la base de donnée";
    } else {
        $message['danger'] = "Erreur pendant l'insertion du produit dans la base de donnée";
    }
}


/* Annuler la modification */
if (isset($_POST['annuler'])) {
    unset($_GET['id']);
}

/* pour un nouveau select dans la base de donnees selon le nom sur lequel on a cliqué */
if (isset($_GET['id'])) {
    $produits = $requete->from('produits')->where('id', $_GET['id'])->fetch();
}


?>

<div class="col-12">
    <div class="card">
        <div class="card-body">
            <?php if (isset($message) && !empty($message)): ?>
                <?php foreach ($message as $key => $value): ?>
                    <div class="bg-<?= $key ?>">
                        <p class="text-white p-2"><?= $message[$key] ?></p>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>

            <div class="card-description">
                Listes des produits
            </div>
            <div class="row">
                <table class="table table-striped table-responsive">
                    <tr>
                        <td>Nom</td>
                        <td>Etat du stock</td>
                        <td>Prix de vente</td>
                        <td>Prix d'achat</td>
                        <td>quantité</td>
                        <td>Photo</td>
                        <td></td>
                    </tr>

                    <?php
                    foreach ($donnees as $row) {

                        ?>
                        <tr>
                            <td> <?php echo '<a href="index.php?page=liste-produit&amp;id=' . $row['id'] . '">' . $row['nom'] . '</a>'; ?></td>
                            <?php if ($row['quantite'] <= 10): ?>
                                <td><label class="badge badge-info">Alerte</label></td>
                            <?php else: ?>
                                <td><label class="badge badge-success">normal</label></td>
                            <?php endif; ?>
                            <td> <?php echo $row['prix_vente']; ?></td>
                            <td> <?php echo $row['prix_achat']; ?></td>
                            <td> <?php echo $row['quantite']; ?></td>
                            <td><img src="../assets/images/produits/<?php echo $row['photo']; ?>" alt=""></td>
                            <td><a class="supprimer" href="#" id="<?= $row['id']; ?>"><i class="fa fa-trash-o fa-2x"></i></a></td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
            <hr>
            <div class="row">
                <?php if (isset($_GET['id'])): ?>

                    <div class="col-6">
                        <h4>Modification du produit </h4>
                        <form class="forms-sample" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nom</label>
                                <input class="form-control" type="text" name="nom" required
                                       value="<?php echo $produits['nom'] ?>"><br>
                            </div>
                            <div class="form-group">
                                <label for="">Prix de Vente:</label>
                                <input class="form-control" type="text" name="prix_v" required
                                       value="<?php echo $produits['prix_vente'] ?>"><br>
                            </div>
                            <div class="form-group">
                                <label for="">Prix d'achat:</label>
                                <input class="form-control" type="text" name="prix_a" required
                                       value="<?php echo $produits['prix_achat'] ?>"><br>
                            </div>

                            <div class="form-group">
                                <label for="categorie">Catégorie:</label>
                                <select class="form-control" name="categ">
                                    <?php foreach ($categories as $id_cat => $cat_nom): ?>
                                        <?php if ($produits['id_categorie'] == $id_cat): ?>
                                            <option value="<?= $id_cat ?>" selected="selected"><?= $cat_nom ?></option>
                                        <?php else: ?>
                                            <option value="<?= $id_cat ?>"><?= $cat_nom ?></option>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="categorie">Marque:</label>
                                <select class="form-control" name="marque">
                                    <?php foreach ($marques as $id_marq => $marq_nom): ?>
                                        <?php if ($produits['id_marque'] == $id_marq): ?>
                                            <option value="<?= $id_marq ?>"
                                                    selected="selected"><?= $marq_nom ?></option>
                                        <?php else: ?>
                                            <option value="<?= $id_marq ?>"><?= $marq_nom ?></option>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Modifier l'image du produit:</label>
                                <div class="input-group">
                                    <input type="file" class="form-control file-upload-info" name="img">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="">Description:</label>
                                <textarea class="form-control" name="desc"
                                          required><?= $produits['description'] ?></textarea><br>
                            </div>

                            <button type="submit" class="btn btn-success mr-2" name="modifier">Modifier</button>
                            <button class="btn btn-light" type="submit" name="annuler">Annuler</button>
                        </form>
                    </div>
                    <div class="col-6">
                        <img class="pt-5" src="../assets/images/produits/<?php echo $produits['photo'] ?>" alt="">
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.supprimer').click(function () {

            var answer = confirm('Êtes vous sure de vouloir supprimer certaine ligne ?');
            if (answer)
            {
                var del_id = $(this).attr('id');
                var $ele = $(this).parent().parent();
                $.ajax({
                    type: 'POST',
                    url: 'produit/delete.php',
                    data: {id: del_id},
                    success: function (data) {
                        if (data == "YES") {
                            $ele.fadeOut().remove();
                        } else {
                            alert("Impossible de supprimer cette ligne.");
                        }
                    }
                });
            }
        })
    });
</script>