<?php
session_start();
require "database.php";

if(isset($_GET['p']) && !empty($_GET['p'])){
    $page = $_GET['p'].'.php';
}else{
    $page = "index.php";
}

$path = 'pages/'.$page;

ob_start();

if(file_exists($path)){
    include "$path";
}else{
    include "pages/404.php";
}

$content = ob_get_contents();
ob_get_clean();

require_once "pages/template.php";
