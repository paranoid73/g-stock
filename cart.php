<?php
session_start();
require "database.php";

if(isset($_GET['action']) && $_GET['action'] == 'ajouter' && !empty($_GET['id_prod'])){
    $id = (int)$_GET['id_prod'];
    if(isset($_SESSION['cart'][$id])){
        $_SESSION['cart'][$id]++;
    }else{
        $_SESSION['cart'][$id] = 1;
    }
    header('location:index.php?p=panier');
}



function total(){
    global $requete;
    $total = 0;
    $ids = array_keys($_SESSION['cart']);
    if(empty($ids)){
        $produits = [];
    }else{
        $d = implode(',',$ids);
        $produits = $requete->from('produits')
                    ->where("(id IN $d)")
                    ->fetchAll();
    }
    foreach ($produits as $pr){
        $total += $pr['prix_vente'];
    }
    return $total;
}